<?php

use yii\db\Migration;

class m160726_054146_init_deal_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'deal',
            [
                'id' => 'pk',
                'leadId' => 'integer',	
				'name' => 'string',
				'amount' => 'integer'			
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
